#!/bin/bash -eux

# Add vagrant user to sudoers.
echo "vagrant        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers

echo 'alias ll="ls -l"' >> /home/vagrant/.bash_aliases

# Disable daily apt unattended updates. Is this needed for Debian?
# echo 'APT::Periodic::Enable "0";' >> /etc/apt/apt.conf.d/10periodic
