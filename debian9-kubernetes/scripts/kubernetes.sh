# Install Kubernetes packages from their official repository
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] http://apt.kubernetes.io
   kubernetes-xenial \
   main"
apt update
apt install -y kubelet kubeadm kubectl
# Create K8S cluster
## Disable swap
swapoff -a
sed -i /swap/d /etc/fstab
## Configure systemd as cgroup driver
bash -c 'cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
'

mkdir -p /etc/systemd/system/docker.service.d
systemctl daemon-reload
systemctl restart docker

echo 'source <(kubectl completion bash)' >> $HOME/.bashrc
