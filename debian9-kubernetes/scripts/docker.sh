#!/bin/bash -eux
apt remove -y docker docker-engine docker.io runc
apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian
   stretch stable \
   stable"
apt update
apt install -y docker-ce
usermod -G docker vagrant