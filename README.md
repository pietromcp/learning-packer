# Pietro Martinelli's Vagrant Box Packer Builds

This project contains the Packer build configurations for all of Pietro Martinelli's (pietrom's) Vagrant Boxes. Each box builds a minimal base box for use with VirtualBox. 
These boxes are based on the wonderful work of Jeff Geerling, as published [here](https://github.com/geerlingguy/packer-boxes).

Available boxes include:

  - [pietrom/debian10](https://app.vagrantup.com/pietrom/boxes/debian10) - [`debian10` directory](debian10/)
  - [pietrom/debian10-docker](https://app.vagrantup.com/pietrom/boxes/debian10-docker) - [`debian10-docker` directory](debian10-docker/)

All of these boxes are available as public, free Vagrant boxes and can be used with the command:

    vagrant init pietrom/[box name here]

You can also fork this repository and customize a build configuration with your own Ansible roles and playbooks to build a fully custom Vagrant box using Packer.

You should refer to the original boxes published by Jeff Geerling as the main *source of truth* for your custom boxes.

## Requirements

The following software must be installed/present on your local machine before you can use Packer to build any of these Vagrant boxes:

  - [Packer](http://www.packer.io/)
  - [Vagrant](http://vagrantup.com/)
  - [VirtualBox](https://www.virtualbox.org/)
  - [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Usage

Make sure all the required software (listed above) is installed, then cd into one of the box directories and run:

    $ packer build -var 'version=1.2.0' box-config.json

After a few minutes, Packer should tell you the box was generated successfully, and the box was uploaded to Vagrant Cloud.

> **Note**: This configuration includes a post-processor that pushes the built box to Vagrant Cloud (which requires a `VAGRANT_CLOUD_TOKEN` environment variable to be set); remove the `vagrant-cloud` post-processor from the Packer template to build the box locally and not push it to Vagrant Cloud. You don't need to specify a `version` variable either, if not using the `vagrant-cloud` post-processor.

## Testing built boxes

There's an included Vagrantfile that allows quick testing of the built Vagrant boxes. From the same box directory, run the following command after building the box:

    $ vagrant up

Test that the box works correctly, then tear it down with:

    $ vagrant destroy -f

## License

[GPL 2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)
